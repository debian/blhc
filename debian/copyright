Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: blhc
Source: https://ruderich.org/simon/blhc/

Files: *
Copyright: 2012-2024 Simon Ruderich <simon@ruderich.org>
License: GPL-3+

Files: bin/blhc
Copyright: 2007-2008 Canonical, Ltd.
           2009-2011 Kees Cook <kees@debian.org>
           2009-2011 Raphaël Hertzog <hertzog@debian.org>
           2009-2017 Guillem Jover <guillem@debian.org>
           2012-2024 Simon Ruderich <simon@ruderich.org>
License: GPL-2+ and GPL-3+
Comment: see scripts/Dpkg/Vendor/Debian.pm in source code of dpkg-1.19.7

Files: debian/*
Copyright: 2012-2017 Jari Aalto <jari.aalto@cante.net>
           2015-2025 Joao Eriberto Mota Filho <eriberto@debian.org>
           2016      gregor herrmann <gregoa@debian.org>
           2018      Fabian Wolff <fabi.wolff@arcor.de>
           2019      Ondřej Nový <onovy@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
