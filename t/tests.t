# Tests for blhc.
#
# Copyright (C) 2012-2024  Simon Ruderich
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


use strict;
use warnings;

use Test::More tests => 248;


sub is_blhc {
    my ($file, $options, $exit, $expected) = @_;

    # Multiple files as array references.
    if (ref $file eq 'ARRAY') {
        local $" = ' ./t/logs/';
        $file = "@{$file}";
    }

    $file = "./t/logs/$file" if $file;
    my $output = `./bin/blhc $options $file 2>&1`;

    if ($options) {
        $options = ' '. $options;
    }
    is $? >> 8, $exit,     "$file$options (exit code)";
    # Perform regex or string match.
    my $cmd = (ref $expected eq 'Regexp')
              ? \&like
              : \&is;
    &$cmd($output, $expected, "$file$options (output)");
}


# Usage, invalid arguments.

use Pod::Usage;
my $usage = ( $Pod::Usage::VERSION < 1.65 ?
        'Usage:
    blhc [*options*] *<dpkg-buildpackage build log file>..*

'
    :
        'Usage:
    blhc [options] <dpkg-buildpackage build log file>..

');
is_blhc '', '--invalid', 2,
        "Unknown option: invalid\n"
        . $usage;

is_blhc '', '', 2,
        $usage;

is_blhc '', '--version', 0,
        'blhc 0.14  Copyright (C) 2012-2024  Simon Ruderich

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
';

is_blhc '', '--help', 1,
        qr/^Usage:
    blhc \[\*?options\*?\] \*?<dpkg-buildpackage build log file>\.\.\*?

Options:
/s;

is_blhc 'doesnt-exist', '', 2,
        qr{^No such file: \./t/logs/doesnt-exist at \./bin/blhc line \d+\.$};


# No compiler commands found.

my $empty = "No compiler commands!\n";
is_blhc 'empty', '', 1,
        $empty;


# ANSI colored output.

is_blhc 'arch-amd64', '--color', 8,
        "\033[31mCFLAGS missing\033[0m (-fstack-protector-strong -fcf-protection)\033[33m:\033[0m gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
\033[31mLDFLAGS missing\033[0m (-pie)\033[33m:\033[0m gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
";


# Ignore missing compiler flags.

is_blhc 'ignore-flag', '--ignore-flag -g', 8,
        'CFLAGS missing (-O2): gcc -g     -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-c.c
';

is_blhc 'ignore-flag', '--ignore-flag -g --ignore-flag -O2', 0,
        '';

is_blhc 'ignore-flag-ldflags', '--ignore-flag -fPIE', 0,
        '';

# Ignore missing compiler flags for specific architectures.

# Invalid option.
is_blhc 'ignore-flag', '--ignore-arch-flag -g', 2,
        'Value "-g" invalid for option ignore-arch-flag ("arch:flag" expected)'
        . "\n$usage";
is_blhc 'ignore-flag', '--ignore-arch-flag -g:', 2,
        'Value "-g:" invalid for option ignore-arch-flag ("arch:flag" expected)'
        . "\n$usage";
is_blhc 'ignore-flag', '--ignore-arch-flag :amd64', 2,
        'Value ":amd64" invalid for option ignore-arch-flag ("arch:flag" expected)'
        . "\n$usage";

# Wrong architecture.
is_blhc 'ignore-flag', '--ignore-arch-flag amd64:-g', 8,
        'CFLAGS missing (-g): gcc    -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-O2): gcc -g     -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-c.c
';

is_blhc 'arch-i386', '--ignore-arch-flag i386:-fstack-protector-strong', 8,
        'LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';
is_blhc 'arch-i386', '--ignore-arch-flag i386:-pie', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
';
is_blhc 'arch-i386', '--ignore-arch-flag i386:-fstack-protector-strong --ignore-arch-flag i386:-pie', 0,
        '';

# Wrong architecture.
is_blhc 'arch-i386', '--ignore-arch-flag amd64:-fstack-protector-strong', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';
is_blhc 'arch-i386', '--ignore-arch-flag amd64:-pie', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';
is_blhc 'arch-i386', '--ignore-arch-flag amd64:-fstack-protector-strong --ignore-arch-flag amd64:-pie', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';

# Ignore certain lines (through inline command).

is_blhc 'ignore-line-inline', '', 8,
        'CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):     ./prepare-script gcc test-a.c test-b.c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):     ./prepare-script gcc test-a.c test-b.c test-c.c
LDFLAGS missing (-Wl,-z,relro):     ./prepare-script gcc test-a.c test-b.c test-c.c
';

is_blhc 'ignore-line-inline2', '', 0,
        '';


# Ignore certain lines.

is_blhc 'ignore-line', '--ignore-line "\./prepare-script gcc test-[a-z]\.c"', 8,
        'CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):     ./prepare-script gcc test-a.c test-b.c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):     ./prepare-script gcc test-a.c test-b.c test-c.c
LDFLAGS missing (-Wl,-z,relro):     ./prepare-script gcc test-a.c test-b.c test-c.c
';

is_blhc 'ignore-line', '--ignore-line "\./prepare-script gcc test-[a-z]\.c" --ignore-line "\s*\./prepare-script gcc test-[a-z]\.c .+"', 0,
        '';

# Ignore certain lines for specific architectures.

# Invalid option.
is_blhc 'ignore-line', '--ignore-arch-line .+', 2,
        'Value ".+" invalid for option ignore-arch-line ("arch:line" expected)'
        . "\n$usage";
is_blhc 'ignore-line', '--ignore-arch-line .+:', 2,
        'Value ".+:" invalid for option ignore-arch-line ("arch:line" expected)'
        . "\n$usage";
is_blhc 'ignore-line', '--ignore-arch-line :amd64', 2,
        'Value ":amd64" invalid for option ignore-arch-line ("arch:line" expected)'
        . "\n$usage";

# Wrong architecture.
is_blhc 'ignore-line', '--ignore-arch-line "amd64:.+"', 8,
        'CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):     ./prepare-script gcc test-a.c test-b.c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):     ./prepare-script gcc test-a.c test-b.c test-c.c
LDFLAGS missing (-Wl,-z,relro):     ./prepare-script gcc test-a.c test-b.c test-c.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): ./prepare-script gcc test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): ./prepare-script gcc test-a.c
LDFLAGS missing (-Wl,-z,relro): ./prepare-script gcc test-a.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): ./prepare-script gcc test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): ./prepare-script gcc test-b.c
LDFLAGS missing (-Wl,-z,relro): ./prepare-script gcc test-b.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): ./prepare-script gcc test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): ./prepare-script gcc test-c.c
LDFLAGS missing (-Wl,-z,relro): ./prepare-script gcc test-c.c
';

# Line regex anchored at beginning/end of the line.
is_blhc 'arch-i386', '--ignore-arch-line "i386:-fPIE"', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';

is_blhc 'arch-i386', '--ignore-arch-line "i386:gcc .+ -fPIE .+ test\.c"', 8,
        'LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';
is_blhc 'arch-i386', '--ignore-arch-line "i386:gcc .+ -Wl,-z,relro -Wl,-z,now .+"', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
';
is_blhc 'arch-i386', '--ignore-arch-line "i386:gcc .+ -fPIE .+ test\.c" --ignore-arch-line "i386:gcc .+ -Wl,-z,relro -Wl,-z,now .+"', 0,
        '';

# Wrong architecture.
is_blhc 'arch-i386', '--ignore-arch-line "amd64:gcc .+ -fPIE .+ test\.c"', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';
is_blhc 'arch-i386', '--ignore-arch-line "amd64:gcc .+ -Wl,-z,relro -Wl,-z,now .+"', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';
is_blhc 'arch-i386', '--ignore-arch-line "amd64:gcc .+ -fPIE .+ test\.c" --ignore-arch-line "amd64:gcc .+ -Wl,-z,relro -Wl,-z,now .+"', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';


# Correct build logs.

is_blhc 'good', '', 0,
        '';
is_blhc 'good-pie', '', 0,
        '';
is_blhc 'good-pie', '--pie', 0,
        '';
is_blhc 'good-bindnow', '', 0,
        '';
is_blhc 'good-bindnow', '--bindnow', 0,
        '';
is_blhc 'good-all', '', 0,
        '';
is_blhc 'good-all', '--all', 0,
        '';
is_blhc 'good-all', '--pie --bindnow', 0,
        '';

is_blhc 'good-multiline', '', 0,
        '';
is_blhc 'good-library', '--all', 0,
        '';


# Build logs with missing flags.

is_blhc 'bad', '', 8,
        'CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-a.c
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-b.c
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-c.c
LDFLAGS missing (-Wl,-z,relro): gcc -o test test-a.o test-b.o test-c.o -ltest
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-a.c
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-b.c
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-c.c
LDFLAGS missing (-Wl,-z,relro): x86_64-linux-gnu-gcc -o test test-a.o test-b.o test-c.o -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
LDFLAGS missing (-Wl,-z,relro): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
LDFLAGS missing (-Wl,-z,relro): gcc -o test test-a.o test-b.o test-c.a
LDFLAGS missing (-Wl,-z,relro): g++  -g -O2 -fstack-protector-strong -Wformat -Werror=format-security ../src/test/objs/test.o -o ../src/test/bin/test
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c -o test test.S
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -E test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -S test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -S test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc test.c
LDFLAGS missing (-Wl,-z,relro): gcc test.c
CXXFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MD -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MD -c test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MT -MD -MF test.d -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MT -MD -MF test.d -c test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MMD -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MMD -c test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MT -MMD -MF test.d -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MT -MMD -MF test.d -c test.c
';
is_blhc 'bad', '--pie', 8,
        'CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-a.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-b.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-c.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): gcc -o test test-a.o test-b.o test-c.o -ltest
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-a.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-b.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-c.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): x86_64-linux-gnu-gcc -o test test-a.o test-b.o test-c.o -ltest
CXXFLAGS missing (-fPIE): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
LDFLAGS missing (-fPIE -pie): g++  -g -O2 -fstack-protector-strong -Wformat -Wformat-security -Werror=format-security -pthread -Wl,-z,relro -o ../src/test/bin/test ../src/test/objs/test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): gcc -o test test-a.o test-b.o test-c.a
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): g++  -g -O2 -fstack-protector-strong -Wformat -Werror=format-security ../src/test/objs/test.o -o ../src/test/bin/test
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c -o test test.S
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -E test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -S test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -S test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc test.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): gcc test.c
CXXFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
LDFLAGS missing (-fPIE -pie): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MD -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MD -c test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MT -MD -MF test.d -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MT -MD -MF test.d -c test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MMD -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MMD -c test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MT -MMD -MF test.d -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MT -MMD -MF test.d -c test.c
';
is_blhc 'bad', '--bindnow', 8,
        'CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-a.c
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-b.c
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-c.c
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc -o test test-a.o test-b.o test-c.o -ltest
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-a.c
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-b.c
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-c.c
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): x86_64-linux-gnu-gcc -o test test-a.o test-b.o test-c.o -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
LDFLAGS missing (-Wl,-z,now): g++  -g -O2 -fstack-protector-strong -Wformat -Wformat-security -Werror=format-security -pthread -Wl,-z,relro -o ../src/test/bin/test ../src/test/objs/test.o
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc -o test test-a.o test-b.o test-c.a
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): g++  -g -O2 -fstack-protector-strong -Wformat -Werror=format-security ../src/test/objs/test.o -o ../src/test/bin/test
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c -o test test.S
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -E test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -S test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -S test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc test.c
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc test.c
CXXFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
LDFLAGS missing (-Wl,-z,now): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MD -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MD -c test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MT -MD -MF test.d -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MT -MD -MF test.d -c test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MMD -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MMD -c test.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MT -MMD -MF test.d -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MT -MMD -MF test.d -c test.c
';
my $bad_pie_bindnow =
        'CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-a.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-b.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -c test-c.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc -o test test-a.o test-b.o test-c.o -ltest
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-a.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-b.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): x86_64-linux-gnu-gcc -g -O2 -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): x86_64-linux-gnu-gcc -g -O2 -c test-c.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): x86_64-linux-gnu-gcc -o test test-a.o test-b.o test-c.o -ltest
CXXFLAGS missing (-fPIE): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): g++  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -pthread -o ./testProgram ../src/test/testProgram.cpp
LDFLAGS missing (-fPIE -pie -Wl,-z,now): g++  -g -O2 -fstack-protector-strong -Wformat -Wformat-security -Werror=format-security -pthread -Wl,-z,relro -o ../src/test/bin/test ../src/test/objs/test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc -o test test-a.o test-b.o test-c.a
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): g++  -g -O2 -fstack-protector-strong -Wformat -Werror=format-security ../src/test/objs/test.o -o ../src/test/bin/test
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c -o test test.S
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -E test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -S test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -S test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc test.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc test.c
CXXFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): gcc -Wl,-z,relro -o test test-.cpp test-b.cpp.o
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MD -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MD -c test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MT -MD -MF test.d -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MT -MD -MF test.d -c test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MMD -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MMD -c test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -MT -MMD -MF test.d -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -MT -MMD -MF test.d -c test.c
';
is_blhc 'bad', '--pie --bindnow', 8,
        $bad_pie_bindnow;
is_blhc 'bad', '--all', 8,
        $bad_pie_bindnow;

is_blhc 'bad-cflags', '', 8,
        'CFLAGS missing (-Wformat): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-fstack-protector-strong): gcc -g -O2 -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-Werror=format-security): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -Wl,-z,relro -o test test.c -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -Wl,-z,relro -o test test.c -ltest
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
CFLAGS missing (-Wformat): gcc -fPIC -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -c -D_FORTIFY_SOURCE=2 ../../../../src/test/test.c -o test.so.o
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc test.c -o test.output
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc test.c -o test.output
LDFLAGS missing (-Wl,-z,relro): gcc test.c -o test.output
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): (gcc -Wl,-z,relro -o test.output test.c)
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): (gcc -Wl,-z,relro -o test.output test.c)
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-comma.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-comma.c
LDFLAGS missing (-Wl,-z,relro):  gcc test-comma.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-and.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-and.c
LDFLAGS missing (-Wl,-z,relro):  gcc test-and.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-or.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-or.c
LDFLAGS missing (-Wl,-z,relro):  gcc test-or.c
';
is_blhc 'bad-cflags', '--pie', 8,
        'CFLAGS missing (-fPIE -Wformat): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-fPIE -fstack-protector-strong): gcc -g -O2 -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-fPIE -Werror=format-security): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.c
LDFLAGS missing (-fPIE -pie): gcc -Wl,-z,relro -o test test-a.o test-b.o test-c.o -ltest
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -Wl,-z,relro -o test test.c -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -Wl,-z,relro -o test test.c -ltest
LDFLAGS missing (-fPIE -pie): gcc -Wl,-z,relro -o test test.c -ltest
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
CFLAGS missing (-Wformat): gcc -fPIC -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -c -D_FORTIFY_SOURCE=2 ../../../../src/test/test.c -o test.so.o
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc test.c -o test.output
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc test.c -o test.output
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): gcc test.c -o test.output
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): (gcc -Wl,-z,relro -o test.output test.c)
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): (gcc -Wl,-z,relro -o test.output test.c)
LDFLAGS missing (-fPIE -pie): (gcc -Wl,-z,relro -o test.output test.c)
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-comma.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-comma.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro):  gcc test-comma.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-and.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-and.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro):  gcc test-and.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-or.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-or.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro):  gcc test-or.c
';
is_blhc 'bad-cflags', '--bindnow', 8,
        'CFLAGS missing (-Wformat): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-fstack-protector-strong): gcc -g -O2 -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-Werror=format-security): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.c
LDFLAGS missing (-Wl,-z,now): gcc -Wl,-z,relro -o test test-a.o test-b.o test-c.o -ltest
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -Wl,-z,relro -o test test.c -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -Wl,-z,relro -o test test.c -ltest
LDFLAGS missing (-Wl,-z,now): gcc -Wl,-z,relro -o test test.c -ltest
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
LDFLAGS missing (-Wl,-z,now): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
CFLAGS missing (-Wformat): gcc -fPIC -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -c -D_FORTIFY_SOURCE=2 ../../../../src/test/test.c -o test.so.o
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc test.c -o test.output
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc test.c -o test.output
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc test.c -o test.output
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): (gcc -Wl,-z,relro -o test.output test.c)
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): (gcc -Wl,-z,relro -o test.output test.c)
LDFLAGS missing (-Wl,-z,now): (gcc -Wl,-z,relro -o test.output test.c)
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-comma.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-comma.c
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now):  gcc test-comma.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-and.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-and.c
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now):  gcc test-and.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-or.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-or.c
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now):  gcc test-or.c
';
is_blhc 'bad-cflags', '--pie --bindnow', 8,
        'CFLAGS missing (-fPIE -Wformat): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-fPIE -fstack-protector-strong): gcc -g -O2 -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-fPIE -Werror=format-security): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.c
LDFLAGS missing (-fPIE -pie -Wl,-z,now): gcc -Wl,-z,relro -o test test-a.o test-b.o test-c.o -ltest
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -Wl,-z,relro -o test test.c -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -Wl,-z,relro -o test test.c -ltest
LDFLAGS missing (-fPIE -pie -Wl,-z,now): gcc -Wl,-z,relro -o test test.c -ltest
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
LDFLAGS missing (-Wl,-z,now): gcc -shared -fPIC -Wl,-z,relro -o test.so test.c -ltest
CFLAGS missing (-Wformat): gcc -fPIC -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -c -D_FORTIFY_SOURCE=2 ../../../../src/test/test.c -o test.so.o
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc test.c -o test.output
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc test.c -o test.output
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc test.c -o test.output
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): (gcc -Wl,-z,relro -o test.output test.c)
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): (gcc -Wl,-z,relro -o test.output test.c)
LDFLAGS missing (-fPIE -pie -Wl,-z,now): (gcc -Wl,-z,relro -o test.output test.c)
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-comma.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-comma.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now):  gcc test-comma.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-and.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-and.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now):  gcc test-and.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc test-or.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc test-or.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now):  gcc test-or.c
';
is_blhc 'bad-cflags-stackprotector', '', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c -fno-stack-protector test-a.c
CFLAGS missing (-fstack-protector-strong): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c -fno-stack-protector-all test-a.c
CFLAGS missing (-fstack-protector-strong): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c -fno-stack-protector-strong test-a.c
';

is_blhc 'bad-cppflags', '', 8,
        'CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -fPIC -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -c ../../../../src/test/test.c -o test.so.o
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++ -o test -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security test-a.cxx test-b.o test-c.o -Wl,-z,relro
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -D_FORTIFY_SOURCE=0 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -D_FORTIFY_SOURCE=1 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=1 -c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=0 -c test-d.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -U_FORTIFY_SOURCE   -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -c test-g.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -U_FORTIFY_SOURCE -c test-i.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=2 -U_FORTIFY_SOURCE -c test-i.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=2 -D_FORTIFY_SOURCE=1 -c test-i.c
';

is_blhc 'bad-cppflags', '--ignore-flag -D_FORTIFY_SOURCE=2', 0,
        '';

my $bad_ldflags =
        'LDFLAGS missing (-Wl,-z,relro): gcc -o test test-a.o test-b.o test-c.o -ltest
LDFLAGS missing (-Wl,-z,relro): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -o test test-a.o test-b.o test-c.o
LDFLAGS missing (-Wl,-z,relro): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -o test test-a.o test-b.o test-c.o test.h
';
is_blhc 'bad-ldflags', '', 8,
        $bad_ldflags;
is_blhc 'bad-ldflags', '--pie', 8,
        'CFLAGS missing (-fPIE): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-fPIE): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-fPIE): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-c.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): gcc -o test test-a.o test-b.o test-c.o -ltest
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -o test test-a.o test-b.o test-c.o
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -o test test-a.o test-b.o test-c.o test.h
';
is_blhc 'bad-ldflags', '--bindnow', 8,
        'LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc -o test test-a.o test-b.o test-c.o -ltest
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -o test test-a.o test-b.o test-c.o
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -o test test-a.o test-b.o test-c.o test.h
';
is_blhc 'bad-ldflags', '--pie --bindnow', 8,
        'CFLAGS missing (-fPIE): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-fPIE): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-fPIE): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-c.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc -o test test-a.o test-b.o test-c.o -ltest
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -o test test-a.o test-b.o test-c.o
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -o test test-a.o test-b.o test-c.o test.h
';

is_blhc 'bad-multiline', '', 8,
        'CFLAGS missing (-Wformat): gcc \         	-g -O2 -fstack-protector-strong\     -fstack-clash-protection -Wformat-security\ -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-fstack-protector-strong): gcc -g -O2 -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security\ -D_FORTIFY_SOURCE=2\ -c test-b.c
CFLAGS missing (-Werror=format-security): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.c
LDFLAGS missing (-Wl,-z,relro): gcc -o\ test test-c.o test-a.o test-b.o\ 	-ltest
LDFLAGS missing (-Wl,-z,relro): gcc -o \ test test-c.o test-b.o test-a.o\ 	
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -o \ test test-b.o test-a.o test-c.c\ 	
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -o \ test test-b.o test-a.o test-c.c\ 	
LDFLAGS missing (-Wl,-z,relro): gcc -o \ test test-b.o test-a.o test-c.c\ 	
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -c test-a.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  gcc -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-Werror=format-security): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat):  gcc -Wformat-security -Werror=format-security -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  gcc -Wformat-security -Werror=format-security -c test-b.c
CFLAGS missing (-O2): gcc -g -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -c test-a.c
CFLAGS missing (-g -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): \ gcc -O2 -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-fstack-protector-strong): gcc -g -O2 -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -c test-a.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):  \ gcc -D_FORTIFY_SOURCE=2 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security \; -Werror=format-security -D_FORTIFY_SOURCE=1 -c test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):  \ gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=1 -c test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c test.c `echo -g -O2 -fstack-protector-strong -fstack-clash-protection echo -Wformat -Wformat-security -Werror=format-security | sed \'s/.../; s/.../\'` -o test.o
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c test.c `echo -g -O2 -fstack-protector-strong -fstack-clash-protection echo -Wformat -Wformat-security -Werror=format-security | sed "s/.../; s/.../"` -o test.o
';

is_blhc 'bad-library', '--all', 8,
        'CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gcc -D_FORTIFY_SOURCE=2 -g -O2 -Wformat -Wformat-security -Werror=format-security test.c -fPIC -DPIC -o libtest.so
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc -D_FORTIFY_SOURCE=2 -g -O2 -Wformat -Wformat-security -Werror=format-security test.c -fPIC -DPIC -o libtest.so
LDFLAGS missing (-Wl,-z,now): gcc -shared -fPIC -DPIC libtest.o -lpthread -O2 -Wl,relro -Wl,--as-needed -o libtest.so
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc -shared -fPIC -DPIC libtest.o -lpthread -O2 -Wl,--as-needed -o libtest.so
LDFLAGS missing (-Wl,-z,now): gcc -shared -fPIC test.o -Wl,-z -Wl,relro -o .libs/libtest.so.1.0.0
LDFLAGS missing (-Wl,-z,relro): gcc -shared -o libtest.so.0d ./test-a.o test/./test-b.o -Wl,-z,now -lpthread -ldl
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): /usr/bin/g++ -shared -fpic -o libtest-6.1.so.0 test.o -ltiff -lz
LDFLAGS missing (-Wl,-z,relro -Wl,-z,now): gcc -Wl,--as-needed  -fPIE -pie -o test.cgi test.o -lgcrypt
CFLAGS missing (-fPIE): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security test.c -o lib`basename test/test`.so
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security test.c -o lib`basename test/test`.so
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security test.c -o lib`basename test/test`.so
';

is_blhc 'env', '--all', 8,
        'CPPFLAGS missing (-D_FORTIFY_SOURCE=2): VERSION="`echo hi`" CPP="gcc -x assembler-with-cpp -E -P -Wdate-time -D_FORTIFY_SOURCE=2" CPPFLAGS="-Wdate-time -D_FORTIFY_SOURCE=2" ../../config/gen-posix-names.sh _SC_ ml_sysconf.h
';


# check the build log is verbose

is_blhc 'verbose-build', '', 12,
        'NONVERBOSE BUILD: checking if you want to see long compiling messages... no
NONVERBOSE BUILD:   CC     libtest-a.lo
NONVERBOSE BUILD:   CC     libtest-b.lo
NONVERBOSE BUILD:   CC     libtest_c.lo
NONVERBOSE BUILD:   CC     libtest-d.lo
NONVERBOSE BUILD:   CCLD   libtest.la
NONVERBOSE BUILD:   LINK   libtest.la
NONVERBOSE BUILD:   CXX    libtest-a.lo
NONVERBOSE BUILD:   CXX    libtest-b.lo
NONVERBOSE BUILD:   CXX    libtest_c.lo
NONVERBOSE BUILD:   CXX    libtest-d.lo
NONVERBOSE BUILD:   CXXLD  libtest.la
NONVERBOSE BUILD:       [CC]   src/test-a.o
NONVERBOSE BUILD:       [CC]   src/test-b.o
NONVERBOSE BUILD:       [CC]   src/test_c.o
NONVERBOSE BUILD:       [CXX]  src/test-d.o
NONVERBOSE BUILD:       [LD]   src/test.o
NONVERBOSE BUILD:       [CC]   src/test-a.o
NONVERBOSE BUILD:       [CC]   src/test-b.o
NONVERBOSE BUILD:       [CC]   src/test_c.o
NONVERBOSE BUILD:       [LD]   src/test.o
NONVERBOSE BUILD:       [CC]   src/test-a.o
NONVERBOSE BUILD:       [CC]   src/test-b.o
NONVERBOSE BUILD:       [CC]   src/test_c.o
NONVERBOSE BUILD:       [LD]   src/test.o
NONVERBOSE BUILD: CC modules/server/test.c
NONVERBOSE BUILD:     C++      test/test.o
NONVERBOSE BUILD: C++ test.cpp
NONVERBOSE BUILD: Building program ../build/bin/test
NONVERBOSE BUILD: Compiling test/test.cc to ../build/test/test.o
NONVERBOSE BUILD: Compiling test/test.cc to ../build/test/test.o
NONVERBOSE BUILD: Building shared library ../build/test/libtest.so.1.2.3
NONVERBOSE BUILD: Compiling test.cc to ../build/test/test.o
NONVERBOSE BUILD: Building program ../build/bin/test
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++ -c -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -o ../build/test/test.o test/test.cc
LDFLAGS missing (-Wl,-z,relro): g++ -fPIC -DPIC \            -o ../build/test/libtest.so.1.2.3 -shared \                       ../build/obj/test/test-a.o ../build/obj/test/test-b.o ../build/obj/test/test-c.o
CXXFLAGS missing (-Wformat): g++ -c -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -o ../build/test/test.o test.cc
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++ -c -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -o ../build/test/test.o test.cc
LDFLAGS missing (-Wl,-z,relro): g++ ../build/obj/test/test.o -o /../build/bin/test
NONVERBOSE BUILD: Compiling test_file.cxx...
CXXFLAGS missing (-fstack-protector-strong -fstack-clash-protection): g++ -g -O2 -fPIC -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test_file.cxx
NONVERBOSE BUILD: [ 22%] Building CXX object src/CMakeFiles/test/test.cpp.o
NONVERBOSE BUILD: [ 82%] Building C object src/CMakeFiles/test/test.c.o
CXXFLAGS missing (-Wformat):  /usr/bin/c++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -o CMakeFiles/test-verbose.dir/verbose.cpp.o -c -D_FORTIFY_SOURCE=2 /tmp/test/src/test-verbose/verbose.cpp
CFLAGS missing (-Werror=format-security):  /usr/bin/gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -o CMakeFiles/test-verbose-c.dir/verbose-c.c.o -c -D_FORTIFY_SOURCE=2 /tmp/test/src/test-verbose-c/verbose-c.c
NONVERBOSE BUILD: Compiling test.c \     gcc test.c
NONVERBOSE BUILD: [  3%] Building CXX object scribus/text/CMakeFiles/scribus_text_lib.dir/frect.cpp.o
NONVERBOSE BUILD: [ 1/13] Compiling src/instance.c
NONVERBOSE BUILD: [11/13] Compiling suil.pc.in
';

is_blhc 'parallel', '', 0, '';


# handle debug builds

is_blhc 'debug-build', '', 0, '';


# configure/make

is_blhc 'configure', '', 1,
        $empty;

is_blhc 'configure-check', '', 4,
        'NONVERBOSE BUILD:   CC              = gcc -std=gnu99 -std=gnu99 test.c
';

is_blhc 'configure-check', '--line-numbers', 4,
        '5:NONVERBOSE BUILD:   CC              = gcc -std=gnu99 -std=gnu99 test.c
';

is_blhc 'make', '', 1,
        $empty;


# qt4

is_blhc 'qt4', '', 1,
        $empty;


# cc

is_blhc 'cc', '--pie --bindnow', 8,
        'CXXFLAGS missing (-fPIE -Wformat): cc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.cc
CFLAGS missing (-fPIE -Wformat): cc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection): cc -g -O2 -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-fPIE -Werror=format-security): cc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): cc -Wl,-z,defs -o test test-a.o test-b.o test-c.o -ltest
CXXFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): cc\     test.cc
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): cc\     test.cc
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): cc\     test.cc
CXXFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): cc\ test.cc
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): cc\ test.cc
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): cc\ test.cc
LDFLAGS missing (-fPIE -pie -Wl,-z,now): cc -Wl,-z,defs test-a.o test-b.o test-c.o -ltest -Wl,-z,relro -o test/test-4.2~_4711/test.so test.o
';


# gcc

is_blhc 'gcc', '--pie --bindnow', 8,
        'CXXFLAGS missing (-fPIE -Wformat): gcc-4.6 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.cc
CFLAGS missing (-fPIE -Wformat): gcc-4.6 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection): gcc-4.6 -g -O2 -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-fPIE -Werror=format-security): gcc-4.6 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc-4.6 -Wl,-z,defs -o test test-a.o test-b.o test-c.o -ltest
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc\     test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc\     test.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc\     test.c
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc\ test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc\ test.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): gcc\ test.c
LDFLAGS missing (-fPIE -pie -Wl,-z,now): gcc-4.6 -Wl,-z,defs test-a.o test-b.o test-c.o -ltest -Wl,-z,relro -o test/test-4.2~_4711/test.so test.o
';


# c++

is_blhc 'c++', '--pie --bindnow', 8,
        'CXXFLAGS missing (-fPIE -Wformat): c++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.cpp
CXXFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection): c++ -g -O2 -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.cpp
CXXFLAGS missing (-fPIE -Werror=format-security): c++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.cpp
CXXFLAGS missing (-fPIE): c++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-d.cc
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): c++ -Wl,-z,defs -o test test-a.o test-b.o test-c.o test-d.o -ltest
LDFLAGS missing (-fPIE -pie -Wl,-z,now): c++ -Wl,-z,defs test-a.o test-b.o test-c.o -ltest -Wl,-z,relro -o test/test-4.2~_4711/test.so test.o
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): c++\     test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): c++\     test.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): c++\     test.c
CXXFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): c++\     test.c++
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): c++\     test.c++
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): c++\     test.c++
CXXFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): c++\ test.c++
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): c++\ test.c++
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): c++\ test.c++
CXXFLAGS missing (-fPIE -Wformat): c++-4.6 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.cpp
CXXFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection): c++-4.6 -g -O2 -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.cpp
CXXFLAGS missing (-fPIE -Werror=format-security): c++-4.6 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.cpp
CXXFLAGS missing (-fPIE): c++-4.6 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-d.cc
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): c++-4.6 -Wl,-z,defs -o test test-a.o test-b.o test-c.o test-d.o -ltest
';


# g++

is_blhc 'g++', '--pie --bindnow', 8,
        'CXXFLAGS missing (-fPIE -Wformat): g++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.cpp
CXXFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection): g++ -g -O2 -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.cpp
CXXFLAGS missing (-fPIE -Werror=format-security): g++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.cpp
CXXFLAGS missing (-fPIE): g++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-d.cc
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): g++ -Wl,-z,defs -o test test-a.o test-b.o test-c.o test-d.o -ltest
CXXFLAGS missing (-fPIE -Wformat): x86_64-linux-gnu-g++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.cpp
CXXFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection): x86_64-linux-gnu-g++ -g -O2 -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.cpp
CXXFLAGS missing (-fPIE -Werror=format-security): x86_64-linux-gnu-g++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.cpp
CXXFLAGS missing (-fPIE): x86_64-linux-gnu-g++ -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-d.cc
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): x86_64-linux-gnu-g++ -Wl,-z,defs -o test test-a.o test-b.o test-c.o test-d.o -ltest
LDFLAGS missing (-fPIE -pie -Wl,-z,now): g++ -Wl,-z,defs test-a.o test-b.o test-c.o -ltest -Wl,-z,relro -o test/test-4.2~_4711/test.so test.o
CFLAGS missing (-g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): g++\     test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++\     test.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): g++\     test.c
CXXFLAGS missing (-fPIE -Wformat): g++-4.6 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-a.cpp
CXXFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection): g++-4.6 -g -O2 -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.cpp
CXXFLAGS missing (-fPIE -Werror=format-security): g++-4.6 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -D_FORTIFY_SOURCE=2 -c test-c.cpp
CXXFLAGS missing (-fPIE): g++-4.6 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-d.cc
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): g++-4.6 -Wl,-z,defs -o test test-a.o test-b.o test-c.o test-d.o -ltest
';


# ada

my $ada = 'CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc-4.6 -c -fPIC -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security test.c
LDFLAGS missing (-Wl,-z,relro): /usr/bin/gcc-4.6 -shared -lgnat-4.6 -o libtest.so.2 test-a.o test-b.o test-c.o -Wl,--as-needed
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -c -g -O2 test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c -g -O2 test.c
CFLAGS missing (-fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -g -O2 test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 test.c
LDFLAGS missing (-fPIE -pie -Wl,-z,relro): gcc -g -O2 test.c
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gcc-6 -c -I./ -I../progs -g -O2 -fPIE -gnat2005 -gnato -gnatVa -fstack-check -gnatw.I -I- -o /«PKGBUILDDIR»/build/objects/arm_frm.o /«PKGBUILDDIR»/progs/arm_frm.adb
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gcc-6 -c -I./ -I../progs -g -O2 -fPIE -gnat2005 -gnato -gnatVa -fstack-check -I- -x ada -o /«PKGBUILDDIR»/build/objects/arm_form.o /«PKGBUILDDIR»/progs/arm_form.ada
';
is_blhc 'ada', '', 8,
        $ada;
is_blhc 'ada-pbuilder', '', 8,
        $ada;


# fortran

is_blhc 'fortran', '', 8,
        'CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132 -Wl,-z,relro -o balls balls.f
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132  -c -o quadric.o quadric.f
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132  -c -o suv.o suv.f
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132 \ rastep.f quadric.o suv.o -Wl,-z,relro \ -o rastep
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132  -c -o render.o render.f
LDFLAGS missing (-Wl,-z,relro): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132 -fstack-protector-strong -fstack-clash-protection -o balls-without-ldflags balls.f
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): mpifort -cpp -DDOUB -g -O2 -fdebug-prefix-map=/«PKGBUILDDIR»=. -c -o transform.o transform.f90
';

is_blhc 'fortran-no-build-deps', '', 8,
        'CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132 -Wl,-z,relro -o balls balls.f
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132  -c -o quadric.o quadric.f
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132  -c -o suv.o suv.f
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132 \ rastep.f quadric.o suv.o -Wl,-z,relro \ -o rastep
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132  -c -o render.o render.f
LDFLAGS missing (-Wl,-z,relro): gfortran -g -w -O2 -Wtabs -ffixed-line-length-132 -fstack-protector-strong -fstack-clash-protection -o balls-without-ldflags balls.f
CFLAGS missing (-fstack-protector-strong -fstack-clash-protection): mpifort -cpp -DDOUB -g -O2 -fdebug-prefix-map=/«PKGBUILDDIR»=. -c -o transform.o transform.f90
';


# libtool

is_blhc 'libtool', '--bindnow', 12,
        'CFLAGS missing (-fPIE -Wformat): libtool: compile: x86_64-linux-gnu-gcc -D_FORTIFY_SOURCE=2 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -c test.c
CXXFLAGS missing (-fPIE -Wformat): libtool: compile: x86_64-linux-gnu-g++ -D_FORTIFY_SOURCE=2 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -c test.cpp
CFLAGS missing (-fPIE -Wformat): libtool: compile: gcc-4.6 -D_FORTIFY_SOURCE=2 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -c test.c
CXXFLAGS missing (-fPIE -Wformat): libtool: compile: g++-4.6 -D_FORTIFY_SOURCE=2 -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat-security -Werror=format-security -c test.cc
LDFLAGS missing (-Wl,-z,now): libtool: link: g++ -shared test-a.o test-b.o test-b.o test-c.o -O2 -Wl,relro -o test.so
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: gcc -Wl,-z -Wl,relro -o test test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: cc -Wl,-z,relro -o test.so test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: gcc-4.6 -Wl,-z,relro -o test.so test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: x86_64-linux-gnu-g++ -Wl,-z,relro -o test.so test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: gcc -Wl,-z,relro -o test.so test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: gcc -Wl,-z,relro -o test/test-4.2~_4711/test.so test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: gcc -Wl,-z,relro -o test.so test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now):   libtool: link: gcc -Wl,-z,relro -o test.so test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: gcc -Wl,-z,relro -o test.so test.o
NONVERBOSE BUILD:  /bin/bash /tmp/test/build/libtool  --silent --tag CC --mode=relink gcc -Wl,-z,relro -o test.so test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now):  libtool: relink: gcc -Wl,-z,relro -o test.so test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now):  libtool: relink: g++ -Wl,-z,relro -o test.la test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: gcc  -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security   -Wl,-z,relro -o test test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -Wl,-z -Wl,relro -o .libs/test test.o
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: g++ -include ./include/CppUTest/MemoryLeakDetectorNewMacros.h -Wall -Wextra -Wshadow -Wswitch-default -Wswitch-enum -Wconversion -pedantic -Wsign-conversion -Woverloaded-virtual -Wno-disabled-macro-expansion -Wno-padded -Wno-global-constructors -Wno-exit-time-destructors -Wno-weak-vtables -Wno-old-style-cast -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -Wl,-z -Wl,relro -o CppUTestTests CppUTestTests-AllocationInCFile.o CppUTestTests-AllocationInCppFile.o CppUTestTests-AllocLetTestFree.o CppUTestTests-AllocLetTestFreeTest.o CppUTestTests-AllTests.o CppUTestTests-CheatSheetTest.o CppUTestTests-CommandLineArgumentsTest.o CppUTestTests-CommandLineTestRunnerTest.o CppUTestTests-JUnitOutputTest.o CppUTestTests-MemoryLeakDetectorTest.o CppUTestTests-MemoryLeakOperatorOverloadsTest.o CppUTestTests-MemoryLeakWarningTest.o CppUTestTests-PluginTest.o CppUTestTests-PreprocessorTest.o CppUTestTests-SetPluginTest.o CppUTest Tests-SimpleStringTest.o CppUTestTests-SimpleMutexTest.o CppUTestTests-TestFailureNaNTest.o CppUTestTests-TestFailureTest.o CppUTestTests-TestFilterTest.o CppUTestTests-TestHarness_cTest.o CppUTestTests-TestHarness_cTestCFile.o CppUTestTests-TestInstallerTest.o CppUTestTests-TestMemoryAllocatorTest.o CppUTestTests-TestOutputTest.o CppUTestTests-TestRegistryTest.o CppUTestTests-TestResultTest.o CppUTestTests-TestUTestMacro.o CppUTestTests-UtestTest.o CppUTestTests-UtestPlatformTest.o  lib/libCppUTest.a -lpthread
LDFLAGS missing (-fPIE -pie -Wl,-z,now): libtool: link: gcc -Wl,-z,relro -o libtest.la test.h test-a.lo test-b.lo test-c.lo test-d.la
LDFLAGS missing (-fPIE -pie -Wl,-z,relro -Wl,-z,now): libtool: link: gcc -o libtest.la test.h test-a.lo test-b.lo test-c.lo test-d.la
NONVERBOSE BUILD: /usr/share/apr-1.0/build/libtool --silent --mode=compile x86_64-linux-gnu-gcc -std=gnu99 -I/usr/include/libxml2 -pthread  -pipe -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security ... -prefer-pic -c mod_buffer.c
NONVERBOSE BUILD: /usr/share/apr-1.0/build/libtool --silent --mode=link x86_64-linux-gnu-gcc -std=gnu99 -I/usr/include/libxml2 -pthread  -pipe -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -Wl,--as-needed -Wl,-z,relro -o mod_buffer.la -rpath /usr/lib/apache2/modules -module -avoid-version  mod_buffer.lo
NONVERBOSE BUILD: /usr/share/apr-1.0/build/libtool --silent --mode=link x86_64-linux-gnu-gcc -std=gnu99  -pthread  -pipe -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -lssl -lcrypto -pie -Wl,--as-needed -Wl,-z,relro -o ab  ab.lo   -lcap        /usr/lib/libaprutil-1.la /usr/lib/libapr-1.la -lm
NONVERBOSE BUILD: /usr/share/apr-1.0/build/libtool --silent --mode=link x86_64-linux-gnu-gcc -std=gnu99  -pthread  -pipe -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -lssl -lcrypto -pie \ 	        -Wl,--as-needed -Wl,-z,relro -o ab  ab.lo   -lcap        /usr/lib/libaprutil-1.la /usr/lib/libapr-1.la -lm
NONVERBOSE BUILD: /bin/bash ../libtool --silent --tag=CC   --mode=compile gcc -DHAVE_CONFIG_H -I. -I..   -D_FORTIFY_SOURCE=2  -Wall -Wextra -Wconversion -g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security -MT coloredstderr.lo -MD -MP -MF .deps/coloredstderr.Tpo -c -o coloredstderr.lo coloredstderr.c
NONVERBOSE BUILD: /bin/bash ../libtool --silent --tag=CC   --mode=link gcc  -Wall -Wextra -Wconversion -g -O2 -fPIE -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security  -fPIE -pie -Wl,-z,relro -Wl,-z,now -o libcoloredstderr.la -rpath /usr/local/lib coloredstderr.lo  -ldl
';

# cargo/rust

is_blhc 'cargo', '', 0,
        '';



# different architectures

my $arch_hppa = '';
is_blhc 'arch-hppa', '', 0,
        $arch_hppa;

my $arch_i386 =
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
';
is_blhc 'arch-i386', '', 8,
        $arch_i386;

my $arch_ia64 = '';
is_blhc 'arch-ia64', '', 0,
        $arch_ia64;

is_blhc 'arch-ia64', '--arch i386', 8,
        'CFLAGS missing (-fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-Wl,-z,relro): gcc -fPIE -pie -o test test.o
CFLAGS missing (-fPIE -fstack-protector-strong): gcc -D_FORTIFY_SOURCE=2 -g -O2 -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie -Wl,-z,relro): gcc -fPIE -o test test.o
';


# architecture in older buildd logs

is_blhc 'dpkg-buildpackage-architecture-old', '', 0,
        '';

# architecture in newer buildd logs

is_blhc 'dpkg-buildpackage-architecture-new', '', 0,
        '';


# correct architecture detection

is_blhc 'buildd-architecture',     '', 0, '';
is_blhc 'buildd-architecture-old', '', 0, '';


# ignore architecture

is_blhc ['arch-hppa', 'arch-i386', 'empty', 'arch-mipsel'],
        '--ignore-arch hppa --ignore-arch mipsel',
        9,
        "checking './t/logs/arch-hppa'...\n"
        . "ignoring architecture 'hppa'\n"
        . "checking './t/logs/arch-i386'...\n"
        . $arch_i386
        . "checking './t/logs/empty'...\n"
        . $empty
        . "checking './t/logs/arch-mipsel'...\n"
        . "ignoring architecture 'mipsel'\n"
        ;

is_blhc 'buildd-dpkg-dev', '--ignore-arch i386', 0,
        "ignoring architecture 'i386'\n";


# debian

is_blhc 'debian', '', 8,
        'CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c `dpkg-buildflags --get CFLAGS` test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++ -c `dpkg-buildflags --get CXXFLAGS` test.cc
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -c `dpkg-buildflags --get LDFLAGS` test.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c `dpkg-buildflags --get LDFLAGS` test.c
LDFLAGS missing (-Wl,-z,relro): gcc -o test test.o `dpkg-buildflags --get CFLAGS`
';

is_blhc 'debian', '--line-numbers', 8,
        '9:CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
13:CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c `dpkg-buildflags --get CFLAGS` test.c
14:CPPFLAGS missing (-D_FORTIFY_SOURCE=2): g++ -c `dpkg-buildflags --get CXXFLAGS` test.cc
15:CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): gcc -c `dpkg-buildflags --get LDFLAGS` test.c
15:CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -c `dpkg-buildflags --get LDFLAGS` test.c
19:LDFLAGS missing (-Wl,-z,relro): gcc -o test test.o `dpkg-buildflags --get CFLAGS`
';

is_blhc 'debian-cmake', '', 32,
        'INVALID CMAKE: 2.8.7-1
';
is_blhc 'debian-cmake-2', '', 32,
        'INVALID CMAKE: 2.8.7-2
';
is_blhc 'debian-cmake-ok', '', 0,
        '';

my $debian_hardening_wrapper =
        'HARDENING WRAPPER: no checks possible, aborting
';
is_blhc 'debian-hardening-wrapper', '', 16,
        $debian_hardening_wrapper;
is_blhc 'debian-hardening-wrapper-old-build-depends', '', 16,
        $debian_hardening_wrapper;
is_blhc 'debian-hardening-wrapper-pbuilder', '', 16,
        $debian_hardening_wrapper;


# false positives

is_blhc 'false-positives', '', 0,
        '';


# buildd support

is_blhc 'empty', '--buildd', 0,
        'I-no-compiler-commands||
';

is_blhc 'buildd-package-details', '--buildd', 0,
        '';

is_blhc 'buildd-dpkg-dev', '--buildd', 0,
        'W-dpkg-buildflags-missing|CPPFLAGS 7 (of 7), CFLAGS 6 (of 6), CXXFLAGS 1 (of 1), LDFLAGS 2 (of 2) missing|
';

# NOTE: 3 of 6 is important here, search for $disable_clash in blhc
is_blhc 'buildd-dpkg-dev-old', '--buildd', 0,
        'W-dpkg-buildflags-missing|CFLAGS 3 (of 6), CXXFLAGS 1 (of 1) missing|
';

is_blhc 'buildd-dpkg-dev-missing', '--buildd', 0,
        'W-dpkg-buildflags-missing|CFLAGS 3 (of 6), CXXFLAGS 1 (of 1) missing|
';

is_blhc 'buildd-gcc-pie', '--buildd --all', 0,
        'W-dpkg-buildflags-missing|CFLAGS 1 (of 1), LDFLAGS 1 (of 1) missing|
';
is_blhc 'buildd-gcc-pie-builtin-wrong-arch', '--buildd --all', 0,
        'W-dpkg-buildflags-missing|CFLAGS 1 (of 1), LDFLAGS 1 (of 1) missing|
';
is_blhc 'buildd-gcc-pie-builtin', '--buildd', 0, '';
is_blhc 'buildd-gcc-pie-builtin', '--buildd --all', 0, '';

# Older dpkg versions use -fstack-protector instead of -strong.
is_blhc 'buildd-dpkg-fstack-protector', '--buildd', 0,
        '';
is_blhc 'buildd-dpkg-fstack-protector-missing', '--buildd', 0,
        'W-dpkg-buildflags-missing|CFLAGS 5 (of 5) missing|
';

is_blhc 'debian-hardening-wrapper', '--buildd', 0,
        'I-hardening-wrapper-used||
';
is_blhc 'debian-hardening-wrapper-pbuilder', '--buildd', 0,
        'I-hardening-wrapper-used||
';

is_blhc 'buildd-verbose-build', '--buildd', 0,
        'W-compiler-flags-hidden|3 (of 5) hidden|
';

is_blhc 'make', '--buildd', 0,
        'I-no-compiler-commands||
';

is_blhc 'debian-cmake', '--buildd', 0,
        'I-invalid-cmake-used|2.8.7-1|
';


# debian specific settings

is_blhc 'debian-gcc-pie', '--debian', 0, '';


# multiple files

is_blhc ['good', 'good-pie', 'good-bindnow', 'good-all', 'good-multiline', 'good-library'], '', 0,
        "checking './t/logs/good'...
checking './t/logs/good-pie'...
checking './t/logs/good-bindnow'...
checking './t/logs/good-all'...
checking './t/logs/good-multiline'...
checking './t/logs/good-library'...
";
is_blhc ['good-all', 'good-library'], '--all', 0,
        "checking './t/logs/good-all'...
checking './t/logs/good-library'...
";

is_blhc ['arch-i386', 'arch-ia64'], '', 8,
        "checking './t/logs/arch-i386'...\n"
        . $arch_i386
        . "checking './t/logs/arch-ia64'...\n"
        . $arch_ia64;

# No exit when multiple files are specified.
is_blhc ['bad-ldflags', 'empty', 'arch-hppa', 'debian-hardening-wrapper'], '', 25,
        "checking './t/logs/bad-ldflags'...\n"
        . $bad_ldflags
        . "checking './t/logs/empty'...\n"
        . $empty
        . "checking './t/logs/arch-hppa'...\n"
        . $arch_hppa
        . "checking './t/logs/debian-hardening-wrapper'...\n"
        . $debian_hardening_wrapper
        ;

# Ignore works correctly with multiple architectures.
is_blhc ['arch-i386', 'arch-amd64', 'arch-hppa', 'ignore-flag'],
        '--ignore-arch-flag i386:-fstack-protector-strong --ignore-arch-flag mipsel:-Werror=format-security', 8,
        "checking './t/logs/arch-i386'...
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
checking './t/logs/arch-amd64'...
CFLAGS missing (-fstack-protector-strong -fcf-protection): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
checking './t/logs/arch-hppa'...
checking './t/logs/ignore-flag'...
CFLAGS missing (-g): gcc    -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-b.c
CFLAGS missing (-O2): gcc -g     -fstack-protector-strong -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -D_FORTIFY_SOURCE=2 -c test-c.c
"
        ;

is_blhc ['arch-i386', 'arch-amd64', 'arch-hppa', 'ignore-line'],
        '--ignore-arch-line "i386:gcc .+ -fPIE .+" --ignore-arch-line "mipsel:gcc .+ -Wl,-z,relro -Wl,-z,now .+"', 8,
        "checking './t/logs/arch-i386'...
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
checking './t/logs/arch-amd64'...
CFLAGS missing (-fstack-protector-strong -fcf-protection): gcc -D_FORTIFY_SOURCE=2 -g -O2 -fPIE -fstack-clash-protection -Wformat -Wformat-security -Werror=format-security -Wall -c test.c
LDFLAGS missing (-pie): gcc -fPIE -Wl,-z,relro -Wl,-z,now -o test test.o
checking './t/logs/arch-hppa'...
checking './t/logs/ignore-line'...
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security):     ./prepare-script gcc test-a.c test-b.c test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2):     ./prepare-script gcc test-a.c test-b.c test-c.c
LDFLAGS missing (-Wl,-z,relro):     ./prepare-script gcc test-a.c test-b.c test-c.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): ./prepare-script gcc test-a.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): ./prepare-script gcc test-a.c
LDFLAGS missing (-Wl,-z,relro): ./prepare-script gcc test-a.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): ./prepare-script gcc test-b.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): ./prepare-script gcc test-b.c
LDFLAGS missing (-Wl,-z,relro): ./prepare-script gcc test-b.c
CFLAGS missing (-g -O2 -fstack-protector-strong -fstack-clash-protection -Wformat -Werror=format-security): ./prepare-script gcc test-c.c
CPPFLAGS missing (-D_FORTIFY_SOURCE=2): ./prepare-script gcc test-c.c
LDFLAGS missing (-Wl,-z,relro): ./prepare-script gcc test-c.c
"
        ;
