README
======

blhc (build log hardening check) is a small tool which checks build logs for
missing hardening flags. It's licensed under the GPL 3 or later.

Hardening flags enable additional security features in the compiler to prevent
e.g. stack overflows, format string vulnerabilities, GOT overwrites, etc.

Because most build systems are quite complicated there are many places where
compiler flags from the environment might be ignored. The parser verifies that
all compiler commands use the correct hardening flags and thus all hardening
features are correctly used.

It's designed to check build logs generated by Debian's dpkg-buildpackage (or
tools using dpkg-buildpackage like pbuilder or sbuild (which is used for the
official buildd build logs)) to help maintainers detect missing hardening
flags in their packages.

At the moment it works only on Debian and derivatives but it should be easily
extendable to other systems as well. Patches are welcome.

Only gcc is detected as compiler at the moment. If other compilers support
hardening flags as well, please report them.

For more information about hardening flags have a look at [1].

[1]: https://wiki.debian.org/Hardening


DEPENDENCIES
------------

- Perl
  - Dpkg::Arch
  - Dpkg::Version
  - Term::ANSIColor >= 2.01
    Bundled with perl. A recent version is only necessary for build logs with
    ANSI colors which is rare, blhc works fine without if the build log
    doesn't use colors. Not required for buildd mode.


USAGE
-----

    blhc path/to/log/file

blhc can be run directly from the source tree (`bin/blhc`) or copied anywhere
on the system. It doesn't have to be explicitly installed. To read the man
page use `perldoc bin/blhc`.

If there's no output, no flags are missing and the build log is fine.

For more examples see the man page.


CHECKS
------

blhc checks all compiler lines (lines matching `gcc`) for hardening flags
(same as set by `dpkg-buildflags`). If a compiler flag is missing a warning
with the missing flags is printed.

Consider the following compiler line:

    gcc -g -O2 -o test test.c

blhc generates the following warnings because all hardening flags are missing:

    CFLAGS missing (-fstack-protector-strong -Wformat -Werror=format-security): gcc -g -O2 -o test test.c
    CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -g -O2 -o test test.c
    LDFLAGS missing (-Wl,-z,relro): gcc -g -O2 -o test test.c

Preprocessing, linking and compiling is automatically detected:

    gcc -MM test.c > test.d
    gcc -E test.c
    gcc -o test test.o

    CPPFLAGS missing (-D_FORTIFY_SOURCE=2): gcc -E test.c
    LDFLAGS missing (-Wl,-z,relro): gcc -o test test.o

blhc differentiates the following flags:

    - CPPFLAGS: preprocessor flags
    - CFLAGS:   C compiler flags
    - CXXFLAGS: C++ compiler flags
    - LDFLAGS:  linker flags

Both '-O2' and '-O3' are recognized as valid, even though only '-O2' is
printed in warnings.

It handles all file extensions as documented by gcc regarding preprocessing,
linking, compiling.

The architecture of the build log is automatically detected by looking for the
following line (output of dpkg-buildpackage):

    dpkg-buildpackage: host architecture <architecture>

The available hardening flags are adapted to the architecture because some
architectures don't support certain hardening options.

Some checks check the build dependencies for certain packages. The following
lines are used to get the build dependencies. The first two are used in buildd
build logs (the second was used in older logs), the third by pbuilder logs,
all are detected:

    Filtered Buildd-Depends: ...
    Build-Depends: ...
    Depends: ...


LIMITATIONS
-----------

The build log must contain the following line which marks the beginning of the
real compile process (output of dpkg-buildpackage):

    dpkg-buildpackage: ...

If it's not present no compiler commands are detected. In case you don't use
dpkg-buildpackage but still want to check a build log, adding it as first line
should work fine.

To prevent false positives when checking debug builds, compiler lines
containing '-OO' or '-Og' are considered debug builds and are not checked for
'-O2', even though fortification doesn't work without '-O2'.

The following non-verbose builds can't be detected:

    gcc -o test

This is not detected because `test` has no file extension. A file extension is
required on a compiler line to prevent many false positives. As the build will
most likely contain other non-verbose build commands (e.g. `gcc test.c`) which
are correctly detected as non-verbose this shouldn't be a problem.

Some CMake non-verbose linker commands are also not correctly detected at the
moment.

blhc still creates a few false positives. Patches to fix them are very welcome
as long as they won't cause any false negatives.


BUILDD MODE
-----------

Buildd mode is enabled if '--buildd' is used. It's designed to check build
logs from Debian's buildds.

Instead of normal warning messages only tags are printed at the end of the
check. See the man page for possible tags.


BUGS
----

If you find any bugs not mentioned in this document please report them to
<simon@ruderich.org> with blhc in the subject.


AUTHORS
-------

Written by Simon Ruderich <simon@ruderich.org>.

Thanks to Bernhard R. Link <brlink@debian.org> and Jaria Alto
<jari.aalto@cante.net> for their valuable input and suggestions.


LICENSE
-------

blhc is licensed under GPL version 3 or later.

Copyright (C) 2012-2024  Simon Ruderich

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

// vim: ft=asciidoc
